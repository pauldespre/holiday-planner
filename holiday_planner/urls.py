from django.conf.urls import patterns, include, url
from employees import views
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'holiday_planner.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.index),
    url(r'^login/', views.login_user),
    url(r'^profile/(?P<user>\w+)/$', views.profile, name='profile'),
    (r'^calendar/', include('django_bootstrap_calendar.urls')),
    url(r'^manager/(?P<user>\w+)/$', views.manager, name='manager'),
)
