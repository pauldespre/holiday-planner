from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse


def index(request):
    """Display the login form on main page"""
    return render(request, 'index.html')


@login_required
def manager(request, user):
    """Display a page for TeamLeader users"""

    return render(request, 'manager.html')


def profile(request, user):
    return render(request, 'profile.html', user)


def login_user(request, context={}):
    """Take the credentials from a POST request and log in the user.
    If login fails, an 'error' key is set in context dict. with an error value.
    Otherwise, if the user is leader, redirect it to /manager/ page,
         if not, redirect to /calendar/ page"""
    logout(request)
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            if user.employee.is_leader:
                url = reverse('manager', kwargs={'user': user})
            else:
                url = '/calendar/'
            return HttpResponseRedirect(url)
        context = {'error': True}
    return render_to_response('index.html', context,
                              context_instance=RequestContext(request))
