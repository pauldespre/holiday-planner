from django.db import models
from django.contrib.auth.models import User


class Employee(models.Model):
    """This class is modeling the Employee table"""

    user = models.OneToOneField(User, primary_key=True)
    name = models.CharField(max_length=30)
    level = models.CharField(max_length=30)  # junior, middle or senior
    join_date = models.DateField()
    email = models.CharField(max_length=50)
    phone = models.CharField(max_length=15, null=True)
    is_leader = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name


class Team(models.Model):
    """This class is modeling the Team table"""

    name = models.CharField(max_length=30)
    team_leader = models.ForeignKey(Employee, null=True)

    def __unicode__(self):
        return self.name


class TeamEmployees(models.Model):
    """This class is modeling the TeamEmployees table"""
    team_id = models.ForeignKey(Team)
    team_employee = models.ForeignKey(Employee, primary_key=True)


class EmployeeVacations(models.Model):
    """This class is modeling the EmployeeVacations table"""
    employee = models.ForeignKey(Employee)
    from_date = models.DateField()
    to_date = models.DateField()
