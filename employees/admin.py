from django.contrib import admin
from models import Employee, Team, TeamEmployees


class AddEmployeeToTeam(admin.TabularInline):
    """Add an Employee to a certain Team"""

    model = TeamEmployees
    verbose_name = 'employee'
    verbose_name_plural = 'Add an employee to this team'
    extra = 1


class RegisterEmployee(admin.ModelAdmin):
    """Create a register form for Employees in the superadmin page"""
    list_display = ('name', 'level', 'join_date')
admin.site.register(Employee, RegisterEmployee)


class RegisterTeam(admin.ModelAdmin):
    """Create a register form for Teams in the superadmin page"""
    list_display = ('name', 'team_leader')
    inlines = [AddEmployeeToTeam]
admin.site.register(Team, RegisterTeam)


